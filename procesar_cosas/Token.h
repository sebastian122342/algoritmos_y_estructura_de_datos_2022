#include <iostream>

using namespace std;

class Token {
    private:
        string value;
        int priority = 0;
    public:
        Token() {}

        Token(string value) {
            this->value = value;
            if (!isNum()) {
                if (!value.compare("+") || !value.compare("-")) {
                    setPriority(1);
                } else if (!value.compare("/") || !value.compare("*")) {
                    setPriority(2);
                } else {
                    setPriority(3);
                }
            }
        }


        void setValue(string v) {
            value = v;
        }

        string getValue() {
            return value;
        }

        bool isNum() {
            for (int x = 0; x < value.length(); x++) {
                if (!isdigit(value[x]))
                    return false;
            }
            return true;
        }

        void setPriority(int x) {
            priority = x;
        }

        int getPriority() {
            return priority;
        }
};