#include <iostream>
#include <cstdlib>
#include <time.h>
#include "Node.h"

/*

mirar al stack dado vuelta, el top es el primero
y solamente se van sacando elementos desde abajo

el1               el1
el2    -------->  el2    ahora el top es el3
el3     pop();    el3
el4

el1                   el1
el2    -------->      el2    ahora el top es el5
el3     push(el5);    el3
el4                   el4
                      el5

Para mostrar los elemementos del stack, se hace de la forma tradicional y
el se mostrara mas arriba mientras que los demas elemenos abajo, al reves
de como se mostro anteriormente.

el1                     el4
el2    -------->        el3    Se mostrara asi
el3   DisplayStack()    el2
el4                     el1


*/

class Stack{
    Node *top;
    public:

        Stack(){
            top = NULL;
        }

        Node *get_top(){
            //Token blank;
            //if (isEmpty())
            //    return blank;
            return top;
        }

        Node *pop(){
            //Token blank;
            //if (isEmpty())
            //    return blank;
            Node *aux = top;
            top = top->get_next();
            return aux;
        }

        void push(Node *new_top){
            new_top->set_next(top);
            top = new_top;
        }

        bool isEmpty(){
            if (top == NULL) return true;
            return false;
        }

        int count() {
            Node *current = top;
            int counter = 0;
            while (current != NULL) {
                counter++;
                current = current->get_next();
            }
            return counter;
        }

        void reverse() {
            Node *guard, *current, *x;
            guard = NULL;
            x = NULL;
            current = top;
            while (current != NULL) {
                current = current->get_next();
                x = pop();
                x->set_next(guard);
                guard = x;
            }
            top = guard;
        }

        void DisplayStack(){
            // mostrar de la forma tradicional, no al reves como se interpreto
            if (isEmpty()){
                cout << "No hay elementos en el stack para mostrar" << endl;
                return;
            }
            Node *current;
            current = top;
            string stack_text = "";

            while (current != NULL){
                stack_text += "|\t"+ current->get_token().getValue() +"\t|"+ "\n";
                current = current->get_next();
            }

            cout << "\nStack" << endl;
            cout << "_________________" << endl;
            cout << stack_text;
            cout << "-----------------" << endl;
        }

        void clean() {
            while (!isEmpty()) {
                pop();
            }
        }
};
