/*sol 2*/

#include <iostream>
#include "Stack.h"


using namespace std;

Node* operate(string str1, string str2, string op){
    float num1 = stof(str1);
    float num2 = stof(str2);
    float res;
    if (op == "+"){
        res = num2 + num1;
    }else if (op == "-"){
        res = num2 - num1;
    }else if (op == "*"){
        res = num2 * num1;
    }else{
        if (num1 == 0){
            cout << "No se puede dividir por 0" << endl;
            exit(0);
        }
        res = num2 / num1;
    }
    Node *n = new Node(Token(to_string(res)));
    return n;
}

Node* process(string input){
    // ARREGLAR LA LOGICA DE ESTE PEDAZO
    Stack stack_nums;
    Stack stack_ops;
    //input = strip_parent(input);
    input += " ";
    //cout << "input: " << input << "size: " << input.length() << endl;
    string aux = "";
    string elem = "";
    string s = "";
    int parenthesis = 0;

    for (int i=0; i < input.length(); i++){
        s += input[i];
        // si no hay parenthesis hacer el proceso normal
        if (parenthesis == 0){
            if (s != " "){
                elem += s;
            }else{
                if (elem == ""){
                    continue;
                }
                Token token(elem);
                Node *n = new Node(token);
                elem = "";
                if (token.getPriority() == 0){
                    stack_nums.push(n);
                }else if (token.getPriority() < 3){
                    if (stack_ops.isEmpty()){
                        stack_ops.push(n);
                    }else{
                        if (token.getPriority() <= stack_ops.get_top()->get_token().getPriority()){
                            string str1 = stack_nums.pop()->get_token().getValue();
                            string str2 = stack_nums.pop()->get_token().getValue();
                            string op = stack_ops.pop()->get_token().getValue();
                            Node *res = operate(str1, str2, op);
                            stack_nums.push(res);
                            stack_ops.push(n);
                        }else{
                            stack_ops.push(n);
                        }
                    }
                }else{
                    //aux += input[i];
                    if (s == "("){
                        parenthesis++;
                    // en teoria este pedazo nunca se deberia ejecutar
                    }else{
                        parenthesis -- ;
                    }
                    if (parenthesis == 0){
                        Node *res = process(aux);
                        stack_nums.push(res);
                        aux = "";
                    }
                }
            }

        // si hay parentesis agregar a string aux
        }else{
            // cambiar el int parentesis en caso de que se agrege uno
            if (s == "("){
                parenthesis++;
            }else if (s == ")"){
                parenthesis--;
            }else{
                aux += input[i];
            }
            if (parenthesis == 0){
                cout << "auxiliar: " << aux << endl;
                Node *res = process(aux);
                stack_nums.push(res);
                aux = "";
            }
        }
        // limpiar s
        s = "";
    }
    while (!stack_ops.isEmpty()){
        string str1 = stack_nums.pop()->get_token().getValue();
        string str2 = stack_nums.pop()->get_token().getValue();
        string op = stack_ops.pop()->get_token().getValue();
        Node *res = operate(str1, str2, op);
        stack_nums.push(res);
    }
    return stack_nums.get_top();
}

string strip_parent(string input){
    // si el en primer indice y en el ultimo indice hay un ( y un )
    // respectivamente, eliminarlos.
    char first;
    first = input[0];
    char last;
    last = input[input.length()-1];
    if (first == '(' && last == ')'){
        input.erase(input.length()-1, 2);
        input.erase(0, 2);
    }
    return input;
}

string buscar(string str, int x){
    string pedazo = "";
    int parent = 0;
    for (int i=x; i<str.length(); i++){
        pedazo += str[i];
        if (str[i] == '('){
            parent += 1;
        }else if (str[i] == ')'){
            parent -= 1;
        }
        if (parent == 0){
            break;
        }
    }
    return strip_parent(pedazo);
}

int main(){
    string input, buscada;
    getline(cin, input);
    cout << "input:" << input << "size:" << input.length() << endl;
    for (int i=0; i < input.length(); i++){
        if (input[i] == '(')
            cout << buscar(input, i) << buscar(input, i).length() <<endl;
    }
    //Node *final = process(input);
    //cout << "resultado final: " << final->get_token().getValue() << endl;

    return 0;
}
