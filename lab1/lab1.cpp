#include <iostream>
//#include <string>
#include <cstdlib>
#include <time.h>

using namespace std;

class Estudiante{
    private:
        string nombre;
        int edad;

    public:
        Estudiante(string nombre, int edad){
            this->nombre = nombre;
            this->edad = edad;
        }

        Estudiante(){
            this->nombre = "";
            this->edad = 0;
        }

        string getNombre(){
            return nombre;
        }

        void setNombre(string nombre){
            this->nombre = nombre;
        }

        int getEdad(){
            return edad;
        }

        void setEdad(int edad){
            this->edad = edad;
        }
};


//Por comodidad, es mejor definir la clase "Nodo" para luego definir la lista
//(cada nodo esta formado por los datos y por su "apuntador" al siguiente)
class NodoEstudiante{
    private:
        Estudiante valor;
        NodoEstudiante *vecino;

    public:
        NodoEstudiante(){
            this->vecino = NULL;
        }

        NodoEstudiante(Estudiante nuevo){
            this->valor.setNombre(nuevo.getNombre());
            this->valor.setEdad(nuevo.getEdad());

            this->vecino = NULL;
        }

        // overloading a set vecino para
        void setVecino(NodoEstudiante *vec){
            this->vecino = vec;
        }
        void setVecino(){
            this->vecino = NULL;
        }

        NodoEstudiante *getVecino(){
            return(this->vecino);
        }

        Estudiante *getEstudiante(){
            return(&this->valor);
        }

};

class ListaEstudiantes{
    private:
        NodoEstudiante *primero, *ultimo;

    public:
        ListaEstudiantes(){
            primero = ultimo = NULL;
        }

        void insertarNodo(NodoEstudiante *nuevo){
            if (ultimo == NULL){
                primero = ultimo = nuevo;
            }else{
                ultimo->setVecino(nuevo);
                ultimo = nuevo;
            }
        }

        void mostrarLista(){
            NodoEstudiante *recorrer;

            recorrer = primero;

            while(recorrer!=NULL){
                Estudiante *estudiante;
                estudiante = recorrer->getEstudiante();

                cout <<"\nEstudiante" << endl;
                cout <<"-- Nombre: " << estudiante->getNombre()  << endl;
                cout <<"-- Edad: " << estudiante->getEdad() << endl;

                recorrer = recorrer->getVecino();
           }
        }

        bool buscarEstudiante(string nombre){
            NodoEstudiante *recorrer;
            recorrer = primero;

            while(recorrer!=NULL){
                Estudiante *estudiante;
                estudiante = recorrer->getEstudiante();

                if (estudiante->getNombre() == nombre){
                    return true;
                }else{
                    recorrer = recorrer->getVecino();
                }
            }
            return false;
        }

        void insertarPrimero(NodoEstudiante *nuevo){
            nuevo->setVecino(primero);
            primero = nuevo;
        }

        void quitarUltimo(){
            if (ultimo != NULL){
                NodoEstudiante *recorrer;
                recorrer = primero;

                while (recorrer!=NULL) {
                    // Esto se ejecuta cuando hay un solo elemento en la lista
                    if (primero == ultimo) {
                        primero = ultimo = NULL;
                        break;
                    } else {

                        if (recorrer->getVecino() == ultimo) {
                            // Esto nunca sera verdadero cuando solo hay un elemento en la lista
                            ultimo = recorrer;
                            ultimo->setVecino();
                            break;
                        }else{
                            recorrer = recorrer->getVecino();
                        }

                    }
                }
            }else{
                cout << "\n\t- No hay nodos disponibles.\n" << endl;
            }
        }

        void quitarPrimero(){
            if (primero != NULL){
                NodoEstudiante *segundo;
                segundo = primero->getVecino();
                if (segundo == NULL){
                    primero = ultimo = NULL;
                }else{
                    primero = segundo;
                }
            }else{
                cout << "no se puede" << endl;
            }
        }
};

NodoEstudiante *create_student(){
    string names[] = {"Pedro", "Juan", "Jose", "Martin", "Carla", "Maria", "Flora"};
    string name = names[rand()%7];
    int age = rand() % 100 + 1;
    return new NodoEstudiante(Estudiante(name, age));
}

int menu(){
    string options[] = {"Agregar nodo", "Mostrar lista", "Buscar estudiante",
                        "Insertar estudiante 1ro", "Quitar primero",
                        "Quitar ultimo"};
    int choice;
    int len = sizeof(options)/sizeof(options[0]);
    cout << endl;
    while (true){
        for (int i=0;i<len;i++){
            cout << "<" << i+1 << "> " << options[i] << endl;
        }
        cout << "<" << len+1 << "> " << "Salir" << endl;
        cout << "Ingrese su opcion: ";
        cin >> choice;
        if (choice >= 0 || choice <= len+1){
            return choice;
        }
    }
}

int main(){
    ListaEstudiantes lista = ListaEstudiantes();
    for (int i=0;i<1;i++){
        lista.insertarNodo(create_student());
    }
    srand(time(NULL));
    while (true){
        int choice = menu();
        switch (choice) {
            case 1:
                lista.insertarNodo(create_student());
                break;
            case 2:
                lista.mostrarLista();
                break;
            case 3:
            {
                string name;
                cout << "Ingrese el nombre a buscar: ";
                cin >> name;
                bool found;
                found = lista.buscarEstudiante(name);
                if (found){
                    cout << "El estudiante se encuentra en la lista" << endl;
                }else{
                    cout << "El estudiante no esta en la lista" << endl;
                }
            }
                break;
            case 4:
                lista.insertarPrimero(create_student());
                break;
            case 5:
                lista.quitarPrimero();
                break;
            case 6:
                lista.quitarUltimo();
                break;
            case 7:
                exit(0);
            default:
                continue;
        }
    }

    return 0;
}
