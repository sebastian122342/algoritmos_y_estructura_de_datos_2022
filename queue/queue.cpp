/*
Queue class
*/

#include <iostream>
#include <cstdlib>
#include <time.h>
#include "node.h"
#include "menu.h"

using std::rand;

class Queue{

    Node *front; // primero en ser servido
    Node *rear; // el que esta ultimo en la cola

    public:
        Queue(){
            front = rear = NULL;
        }

        void enqueue(Node *new_node){
            if (isEmpty()){
                rear = front = new_node;
                front->set_next();
            }else{
                new_node->set_next(rear);
                rear = new_node;
            }
        }

        Student dequeue(){
            if (isEmpty()){
                Student blank;
                return blank;
            }else if (front == rear){
                front = rear = NULL;
                Student blank;
                return blank;
            }else{
                Node *current, *aux;
                aux = front;
                current = rear;
                while (current != NULL){
                    if (current->get_next() == front) break;
                    current = current->get_next();
                }
                current->set_next();
                front = current;
                return aux->get_student();
            }
        }

        bool isEmpty(){
            if (front == NULL) return true;
            return false;
        }

        void DisplayQueue(){
            if (isEmpty()){
                cout << "No hay data para mostrar" << endl;
                return;
            }
            string queue_text = "";
            Node *current;
            current = rear;
            while (current != NULL){
                queue_text = " <- " + current->get_student().get_name() + queue_text;
                current = current->get_next();
            }
            queue_text.erase(0, 4);
            cout << "Queue" << endl;
            cout << queue_text << endl;
        }
};

Node *create_student(){
    string names[] = {"Pedro", "Juan", "Jose", "Martin", "Carla", "Maria", "Flora"};
    string name = names[rand()%7];
    int age = rand() % 100 + 1;
    return new Node(Student(name, age));
}

int main(){
    srand(time(NULL));
    int queue_size = 10;
    string options[] = {"Agregar a la cola", "Eliminar la punta",
                        "Mostrar Queue", "Salir"};
    Menu menu(options);
    Queue queue;

    // inicializa la queue
    for (int i=0; i < queue_size; i++)
        queue.enqueue(create_student());

    while (true){
        menu.show_options();
        int option = menu.ask_option();

        switch (option){
            case 1:
                queue.enqueue(create_student());
                break;
            case 2:
            {
                Student student = queue.dequeue();
                if (!student.is_blank())
                    cout << "Se he eliminado a " << student.get_name() << endl;
                else
                    cout << "No hay nadie para eliminar" << endl;
                break;
            }
            case 3:
                queue.DisplayQueue();
                break;
            case 4:
                exit(0);
            default:
                continue;
        }
    }

    return 0;
}
