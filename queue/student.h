/*
Student
*/

#include <string>

using std::string;

class Student{
    string name;
    int age;

	public:
        Student(string name, int age){
            this->name = name;
            this->age = age;
        }

        Student(){
            this->name = "";
            this->age = 0;
        }

        string get_name(){
            return name;
        }

        void set_name(string name){
            this->name = name;
        }

        int get_age(){
            return age;
        }

        void set_age(int age){
            this->age = age;
        }

		bool is_blank(){
			if (name == "")
				return true;
			return false;
		}
};
