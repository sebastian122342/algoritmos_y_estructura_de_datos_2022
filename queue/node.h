/*
Node class
*/

#include "student.h"

class Node{
    Student student;
    Node *next;

    public:
        Node(){
            next = NULL;
        }
        Node(Student student){
            this->student = student;
            this->next = NULL;
        }
        Node *get_next(){
            return next;
        }
        void set_next(Node *next){
            this->next = next;
        }
        void set_next(){
            next = NULL;
        }
        Student get_student(){
            return student;
        }
        void set_student(Student student){
            this->student = student;
        }
};
