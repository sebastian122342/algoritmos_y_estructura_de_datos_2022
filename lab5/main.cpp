// Laboratorio 5
// Integrantes: Sebastian Bustamante, Magdalena Lolas y Diego Nuñez

#include "grafo.cpp"

int main(){

    // Se crea el grafo de la guia
    int tam = 9;
    string caracteres[tam] = {"a", "b", "c", "d", "e", "f", "g", "h", "i"};
    GrafoTipo<string> x(tam);
    for (int i=0; i < tam; i++)
        x.AgregaVertice(caracteres[i]);
    x.AgregaArista("a", "b", 10);
    x.AgregaArista("a", "e", 1);
    x.AgregaArista("a", "d", 2);
    x.AgregaArista("b", "c", 3);
    x.AgregaArista("c", "b", 4);
    x.AgregaArista("d", "e", 0);
    x.AgregaArista("d", "g", 6);
    x.AgregaArista("d", "d", 4);
    x.AgregaArista("e", "f", 7);
    x.AgregaArista("e", "h", 1);
    x.AgregaArista("e", "b", 8);
    x.AgregaArista("f", "c", 8);
    x.AgregaArista("f", "b", 2);
    x.AgregaArista("g", "h", 2);
    x.AgregaArista("h", "f", 2);
    x.AgregaArista("h", "g", 5);
    x.AgregaArista("h", "d", 9);
    x.AgregaArista("i", "h", 6);
    x.AgregaArista("i", "f", 7);

    // Se aplica el algoritmo de dijkstra con "a" como el origen
    string origen = "a";
    x.dijkstra(origen);

    return 0;
}
