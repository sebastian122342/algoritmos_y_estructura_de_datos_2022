// Clase GrafoTipo con constantes a utilizar

const int NULL_ARISTA = 0;
const int INF = 999999;

template <class T>
class GrafoTipo {
	public:
        GrafoTipo(int);
        ~GrafoTipo();
        void AgregaVertice(T);
        void AgregaArista(T, T, int);
        int CostoEs(T, T);
        int IndiceEs(T*, T);
        bool EstaVacio();
        bool EstaLleno();
        int TamanioGrafo();
        void MostrarSolucion();
        int DistMinima();
        void dijkstra(T);

    private:
        int numVertices;
        int maxVertices;

        T* vertices;
        int **aristas;

        bool* marcas;
        int *dists;
};
