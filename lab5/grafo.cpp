// Metodos de la clase GrafoTipo

#include "grafo.h"
#include <iostream>
using namespace std;

// Constructor
template <class T>
GrafoTipo<T>::GrafoTipo(int maxV){
    numVertices = 0;
    maxVertices = maxV;
    vertices = new T[maxV];
    aristas = new int*[maxV];

    for(int i=0; i < maxV; i++)
        aristas[i] = new int[maxV];

    marcas = new bool[maxV];
    dists = new int[maxV];
}

// Destructor
template <class T>
GrafoTipo<T>::~GrafoTipo(){
    delete [] vertices;

    for(int i=0; i < maxVertices; i++)
        delete [] aristas[i];

    delete [] aristas;
    delete [] marcas;
    delete [] dists;
}

// Agrega un vertice al grafo
template <class T>
void GrafoTipo<T>::AgregaVertice(T vertice){
    vertices[numVertices] = vertice;
    for(int indice = 0; indice < numVertices; indice++){
        aristas[numVertices][indice] = NULL_ARISTA;
        aristas[indice][numVertices] = NULL_ARISTA;
    }

    numVertices++;
}

// Se agrega una arista entre dos vertices, con un peso
template <class T>
void GrafoTipo<T>::AgregaArista(T desdeV, T hastaV, int costo){
    int fila, col;

    fila = IndiceEs(vertices, desdeV);
    col = IndiceEs(vertices, hastaV);
    aristas[fila][col] = costo;
}

// Peso de la arista entre dos vertices
template <class T>
int GrafoTipo<T>::CostoEs(T desdeV, T hastaV){
    int fila, col;

    fila = IndiceEs(vertices, desdeV);
    col = IndiceEs(vertices, hastaV);

    return aristas[fila][col];
}

// Indice de un vertice en la lista de vertices
template <class T>
int GrafoTipo<T>::IndiceEs(T *vertices, T vertice){
    for(int i=0; i < numVertices; i++){
        if (vertices[i] == vertice)
            return i;
    }

    return -1;
}

// Comprueba si el grafo esta lleno
template <class T>
bool GrafoTipo<T>::EstaLleno(){
    if (numVertices == maxVertices)
        return true;
    return false;
}

// Comprueba si el grafo esta vacio
template <class T>
bool GrafoTipo<T>::EstaVacio(){
    if (numVertices == 0)
        return true;
    return false;
}

// Retorna el numero de vertices del grafo
template <class T>
int GrafoTipo<T>::TamanioGrafo(){
    return numVertices;
}

// Funcion auxiliar del algoritmo de dijkstra, muestra la distancia mas
// corta entre el origen y cada uno de los vertices
template <class T>
void GrafoTipo<T>::MostrarSolucion(){
    string nombre, costo;
    cout << "Vertice\tCamino mas corto" << endl;
    for (int i=0; i < maxVertices; i++){
        if (i < numVertices){
            nombre = vertices[i];
        }else{
            nombre = "--";
        }
        if (dists[i] == INF)
            costo = "∞";
        else
            costo = to_string(dists[i]);
        cout << nombre << "\t" << costo << endl;
    }
}

// Funcion auxiliar del algoritmo de dijkstra, retorna el indice con menor
// costo, de la lista de vertices aun sin procesar
template <class T>
int GrafoTipo<T>::DistMinima(){
    int min = INF, ind_minimo;
    for (int i=0; i < maxVertices; i++)
        if (!marcas[i] && dists[i] <= min)
            min = dists[i], ind_minimo = i;
    return ind_minimo;
}

// Aplica el algoritmo de dijkstra a partir de un origen
template <class T>
void GrafoTipo<T>::dijkstra(T nodo_origen){
    int ind_raiz = IndiceEs(vertices, nodo_origen);
    // inicializar las listas
    for (int i=0; i < maxVertices; i++)
        dists[i] = INF, marcas[i] = false;
    dists[ind_raiz] = 0;

    for (int j=0; j < maxVertices-1; j++){
        int aux = DistMinima();
        marcas[aux] = true;
        for (int i=0; i < maxVertices; i++)
            if (!marcas[i] && aristas[aux][i] && dists[aux]!= INF
                && dists[aux] + aristas[aux][i] < dists[i])
                dists[i] = dists[aux] + aristas[aux][i];
    }
    cout << "Desde: " << vertices[ind_raiz] << endl;
    MostrarSolucion();
}
