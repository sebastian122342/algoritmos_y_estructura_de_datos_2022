/*
Aqui voy a hacer cosas propias para practicar y asi

*/

#include <iostream>

/* Esto es estrictamente mala practica por posibles conflictos de nombres
al colocarlo aca afuera va a afectar a todo el programa
si se coloca dentro de una funcion solo afectara a esa funcion
esto tambien sirve para el endl*/
using namespace std;

// using std::cout; --> mejor

// declaracion de la funcion
// se puede tener solo defincion aca arriba igual, funciona como ambas cosas
void buenas_tardes();

int main(){

    cout << "hola amigos" << endl;
    // no importa la redeclaracion de la variable number porque estan en dos
    // scopes diferentes (dos {} distintos)
    int number = 3;
    cout << number << endl;

    {
        cout << "le peps" << endl;
        int number = 5;
        cout << number << endl;
    }

    /* si en el cin se mete una string o cosa diferente de int va a mostrar 0
    el cout */
    int hora;
    cout << "q hora es: ";
    cin >> hora;
    cout << "son las " << hora << endl;
    buenas_tardes();

    return 0;
}

// definicion de la funcion
void buenas_tardes(){
    cout << "Buenas tardes amigos" << endl;
}
