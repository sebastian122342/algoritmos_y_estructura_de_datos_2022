// Declaracion de clase grafo con constantes a utilizar

const int NULL_ARISTA = 0;
const int INF = 999999;

template <class T>
class GrafoTipo {
	public:
        GrafoTipo(int);
        ~GrafoTipo();
        void AgregaVertice(T);
        void AgregaArista(T, T, int);
        int CostoEs(T, T);
        int IndiceEs(T*, T);
        bool EstaVacio();
        bool EstaLleno();
        int TamanioGrafo();


        void costo_minimo(T, T);
        
        bool esVecino(T, T);


    private:
        int numVertices;
        int maxVertices;

        T* vertices;
        int **aristas;

        bool* marcas;
		int mas_barato;
		void dfs(int, int, bool*, int*, int&);
};
