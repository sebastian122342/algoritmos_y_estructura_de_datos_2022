#include "grafo.h"
#include <iostream>

using namespace std;

// Constructor
template <class T>
GrafoTipo<T>::GrafoTipo(int maxV){
    numVertices = 0;
    maxVertices = maxV;
    vertices = new T[maxV];
    aristas = new int*[maxV];

    for(int i=0; i < maxV; i++)
        aristas[i] = new int[maxV];

    marcas = new bool[maxV];
}

// Destructor
template <class T>
GrafoTipo<T>::~GrafoTipo(){
    delete [] vertices;

    for(int i=0; i < maxVertices; i++)
        delete [] aristas[i];

    delete [] aristas;
    delete [] marcas;
}

// Agrega un vertice al grafo
template <class T>
void GrafoTipo<T>::AgregaVertice(T vertice){
    vertices[numVertices] = vertice;
    for(int indice = 0; indice < numVertices; indice++){
        aristas[numVertices][indice] = NULL_ARISTA;
        aristas[indice][numVertices] = NULL_ARISTA;
    }

    numVertices++;
}

// Agrega una arista uniendo dos nodos, con un costo
template <class T>
void GrafoTipo<T>::AgregaArista(T desdeV, T hastaV, int costo){
    int fila, col;

    fila = IndiceEs(vertices, desdeV);
    col = IndiceEs(vertices, hastaV);
    aristas[fila][col] = costo;
    aristas[col][fila] = costo;
}

// Retorna el peso de la arista entre dos nodos
template <class T>
int GrafoTipo<T>::CostoEs(T desdeV, T hastaV){
    int fila, col;

    fila = IndiceEs(vertices, desdeV);
    col = IndiceEs(vertices, hastaV);

    return aristas[fila][col];
}

// Retorna el indice de un vertice del grafo
template <class T>
int GrafoTipo<T>::IndiceEs(T *vertices, T vertice){
    for(int i=0; i < numVertices; i++){
        if (vertices[i] == vertice)
            return i;
    }

    return -1;
}

// Comprueba si el grafo esta lleno
template <class T>
bool GrafoTipo<T>::EstaLleno(){
    if (numVertices == maxVertices)
        return true;
    return false;
}

// Comprueba si el grafo esta vacio
template <class T>
bool GrafoTipo<T>::EstaVacio(){
    if (numVertices == 0)
        return true;
    return false;
}

// Tamaño del grafo
template <class T>
int GrafoTipo<T>::TamanioGrafo(){
    return numVertices;
}


// Mostrar el precio mas barato entre dos nodos
template <class T>
void GrafoTipo<T>::costo_minimo(T origen, T destino){

    // Se debe almacenar los nodos visitados
    bool* visitados = new bool[maxVertices];
 
    // Se crea un arreglo para guardar la ruta
    int* ruta = new int[maxVertices];
    int indice_ruta = 0; // La ruta empieza vacia

    // Todos los vertices empiezan como no visitados
    for (int i = 0; i < maxVertices; i++)
        visitados[i] = false;

    mas_barato = INF;

    int indice_origen = IndiceEs(vertices, origen);
    int indice_destino = IndiceEs(vertices, destino);
 
    // Se llama a la funcion recursiva para que busque la ruta de menor costo
    dfs(indice_origen, indice_destino, visitados, ruta, indice_ruta);

    cout << "El costo minimo desde " << origen << " hasta " << destino << " es: " << mas_barato << endl;
}
 
// Funcion recursiva que busca el camino mas corto desde el origen hasta el destino.
// visitados[] hace seguimiento de los vertices ya visitados por la ruta actual.
// ruta[] almacena los vertices y indice_ruta es el indice actual de la ruta.
template <class T>
void GrafoTipo<T>::dfs(int origen, int destino, bool visitados[],
                              int ruta[], int& indice_ruta)
{
    // Se visita el nodo actual y se añade a la ruta
    visitados[origen] = true;
    ruta[indice_ruta] = origen;
    indice_ruta++;
 
    // Si el vertice actual es el destino, se calcula el costo de la ruta.
    int suma = 0;
    if (origen == destino) {
        for (int j=0; j < indice_ruta-1; j++){
            suma += CostoEs(vertices[ruta[j]], vertices[ruta[j+1]]);
        }
        // Si el costo de la ruta es menor o igual a otra ruta 
        // previamente recorrida, guarda la actual como la mas barata
        if (suma <= mas_barato)
            mas_barato = suma;
    }
    else // Si el vertice actual no es el destino
    {
        // Se hace la recursion con todos los nodos conectados al actual
        // para examinar todas las rutas posibles

        for (int iter=0; iter < maxVertices; iter++) {
            if (esVecino(vertices[origen], vertices[iter])) {
                if (!visitados[iter])
                    dfs(iter, destino, visitados, ruta, indice_ruta);
            }
        }
    }
 
    // Se elimina el nodo actual de la ruta y se marca como no visitado.
    indice_ruta--;
    visitados[origen] = false;
}

// Verifica si dos nodos son adyacentes
template <class T>
bool GrafoTipo<T>::esVecino(T raiz, T obj){
    if (CostoEs(raiz, obj) != 0)
        return true;
    return false;
}
