// Laboratorio 4

#include "grafo.cpp"

int main(){

    // Se crea un grafo con diferentes ciudades. El grafo es de tipo String
    int tam = 10;
    GrafoTipo<string> y(tam);
    y.AgregaVertice("Kansas");
    y.AgregaVertice("Talca");
    y.AgregaVertice("Paris");
    y.AgregaVertice("Constitucion");
    y.AgregaVertice("Rancagua");
    y.AgregaVertice("Cumpeo");
    y.AgregaVertice("Santiago");
    y.AgregaVertice("Londres");
    y.AgregaArista("Kansas", "Talca", 1000);
    y.AgregaArista("Talca", "Paris", 500);
    y.AgregaArista("Santiago", "Kansas", 2500);
    y.AgregaArista("Talca", "Santiago", 3500);
    y.AgregaArista("Santiago", "Paris", 3000);
    y.AgregaArista("Londres", "Paris", 700);
    y.AgregaArista("Constitucion", "Paris", 420);
    y.AgregaArista("Talca", "Cumpeo", 580);
    y.AgregaArista("Rancagua", "Londres", 6500);
    y.AgregaArista("Rancagua", "Cumpeo", 750);
    y.AgregaArista("Cumpeo", "Londres", 690);
    y.AgregaArista("Rancagua", "Constitucion", 200);


    y.costo_minimo("Constitucion", "Santiago");


    // Se crea un grafo con numeros del 1 al 10. El grafo es de tipo int
    GrafoTipo<int> i(tam);
    for (int iter=0; iter<tam; iter++)
        i.AgregaVertice(iter+1);
    
    i.AgregaArista(1, 2, 10);
    i.AgregaArista(1, 3, 20);
    i.AgregaArista(3, 4, 15);
    i.AgregaArista(3, 7, 10);
    i.AgregaArista(4, 5, 1);
    i.AgregaArista(5, 7, 11);
    i.AgregaArista(5, 9, 4);
    i.AgregaArista(6, 8, 5);
    i.AgregaArista(4, 6, 7);
    i.AgregaArista(6, 7, 17);
    i.AgregaArista(8, 10, 30);
    i.AgregaArista(9, 10, 9);
    i.AgregaArista(2, 6, 9);

    i.costo_minimo(1, 9);


    return 0;
}
