/*
Ejercicio en clases de listas enlazadas

Hacer la clase University
Hacer la clase Node
Hacer la clase List

*/

#include <iostream>
#include <string>
#include <cstdlib>
#include <time.h>

using std::cout;
using std::endl;
using std::string;

class University{

    string name;
    string address;

    public:
        University(string name, string address){
            this->name = name;
            this->address = address;
        }

        University(){
            name = "";
            address = "";
        }

        void set_name(string name){
            this->name = name;
        }
        void set_address(string address){
            this->address = address;
        }

        string get_name(){
            return name;
        }
        string get_address(){
            return address;
        }
};

class Node{
    University uni;
    Node *next;

    public:
        Node(){
            next = NULL;
        }
        Node(University uni){
            this->uni = uni;

            /*
            ¿this->uni.set_name(uni.get_name)
            this->uni.set_address(uni.get_address)?
            */

            this->next = NULL;
        }
        Node *get_next(){
            return next;
        }
        void set_next(Node *next){
            this->next = next;
        }
        void set_next(){
            next =  NULL;
        }
        University get_uni(){
            return uni;
        }
        void set_uni(University uni){
            this->uni = uni;
        }
};

class LinkedList{
    Node *first, *last;

    public:
        LinkedList(){
            first = last = NULL;
        }

        void append(Node *node){
            if (last == NULL){
                first = last = node;
            }else{
                last->set_next(node);
                last = node;
            }
        }

        bool search_for(string name){
            Node *current;
            current = first;
            while (current!=NULL){
                if (current->get_uni().get_name() == name){
                    return true;
                }else{
                    return false;
                }
            }
        }

        void show(){
            Node *current;
            current = first;

            cout << "Universidades:\n" << endl;

            while (current != NULL){
                cout << "Nombre: " << current->get_uni().get_name() << "\n" << endl;
                //cout << "Direccion: " << current->get_uni().get_address() << "\n" << endl;

                current = current->get_next();
            }
        }

        void delete_first(){
            Node *second;
            if (first!=NULL){

            }else{
                cout << "No se puede" << endl;
            }
        }

        void pop(){
            if (last = NULL){
                cout << "No es posible" << endl;
            }else{
                Node *current;
                current = first;

                if (first = last){
                    first = last = NULL;
                    return;
                }

                while (current != NULL){
                    if (current->get_next() == last){
                        last = current;
                        last->set_next();
                        break;
                    }else{
                        current = current->get_next();
                    }
                }
            }
        }

};

Node *create_uni(){
    string names[] = {"Utal", "ucm", "uchile", "usach", "uautonoma"};
    int len = *(&names + 1) - names;
    int index = rand()%len;
    string name = names[];
    string address = "Hola";
    return new Node(University(name, address));
}

int main(){

    University utal("Universidad de talca", "Lircay o asi");
    LinkedList list;
    srand(time(NULL));
    for (int i = 0; i < 5; i++){
        list.append(create_uni());
    }
    list.show();

    return 0;
}
