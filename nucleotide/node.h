/*
Node class
*/

#include "nucleotide.h"

class Node{
    Nucleotide nuc;
    Node *next;

    public:
        Node(){
            next = NULL;
        }
        Node(Nucleotide nuc){
            this->nuc = nuc;
            this->next = NULL;
        }
        Node *get_next(){
            return next;
        }
        void set_next(Node *next){
            this->next = next;
        }
        void set_next(){
            next = NULL;
        }
        Nucleotide get_nucleotide(){
            return nuc;
        }
        void set_nucleotide(Nucleotide nuc){
            this->nuc = nuc;
        }
};
