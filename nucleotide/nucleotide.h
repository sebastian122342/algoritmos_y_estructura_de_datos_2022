/*
class nucleotide
*/

#include <string>

using std::string;

class Nucleotide{
    string name;

    public:
        Nucleotide(string name){
            this->name =  name;
        }
        Nucleotide(){
            name = "";
        }
        void set_name(string name){
            this->name = name;
        }
        string get_name(){
            return name;
        }

        string get_comp(){
            if (name == "A"){
                return "T";
            }else if (name == "T"){
                return "A";
            }else if (name == "C"){
                return "G";
            }else if (name == "G"){
                return "C";
            }else{
                return "";
            }
        }

        bool is_blank(){
            if (name == "") return true;
            return false;
        }
};
