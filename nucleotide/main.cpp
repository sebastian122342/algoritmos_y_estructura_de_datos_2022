/*
Hacer una cola que sea una serie de objetos tipo nucleotide
a lo mejor ir agregando despues cosa de propiedades de los nucleotidos a
partir de como se van armando y asi
*/

#include <iostream>
#include <cstdlib>
#include <time.h>
#include "menu.h"
#include "dna.h"

using std::rand;

Node *create_nucleotide(){
    string names[] = {"A", "T", "C", "G"};
    string name = names[rand()%4];
    //int age = rand() % 100 + 1;
    return new Node(Nucleotide(name));
}

int main(){

    srand(time(NULL));

    Stack dna;
    int primer = 18;
    for (int i=0; i<primer; i++)
        dna.push(create_nucleotide());
    dna.DisplayStack();

    string options[] = {"Agregar nucleotido", "Eliminar Nucleotido",
                    "Mostrar cadena", "Mostrar complementaria", "Salir"};
    Menu menu(options);
    while (true){
        menu.show_options();
        int option = menu.ask_option();
        switch (option){
            case 1:
                dna.push(create_nucleotide());
                break;
            case 2:
                dna.pop();
                break;
            case 3:
                dna.DisplayStack();
                break;
            case 4:
                dna.DisplayComp();
                break;
            case 5:
                exit(0);
            default:
                continue;
        }
    }


    return 0;
}
