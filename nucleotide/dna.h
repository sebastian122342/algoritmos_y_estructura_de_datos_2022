/*
class queue para representar el dna

*/

#include <iostream>
#include "node.h"

using std::cout;

class Stack{
    Node *top;
    public:
        Stack(){
            top = NULL;
        }

        Nucleotide get_top(){
            Nucleotide blank;
            if (isEmpty())
                return blank;
            return top->get_nucleotide();
        }

        void pop(){
            if (isEmpty())
                return;
            Node *aux = top;
            top = top->get_next();
            delete aux;
        }

        void push(Node *new_top){
            new_top->set_next(top);
            top = new_top;
        }

        bool isEmpty(){
            if (top == NULL) return true;
            return false;
        }

        void DisplayStack(){
            // mostrar de la forma tradicional, no al reves como se interpreto
            if (isEmpty()){
                cout << "No hay elementos en el stack para mostrar" << endl;
                return;
            }
            Node *current;
            current = top;
            string stack_text = "";

            while (current != NULL){
                stack_text = current->get_nucleotide().get_name() + stack_text;
                current = current->get_next();
            }

            cout << "\nDNA\n" << endl;
            cout << stack_text << "\n" << endl;
        }

        void DisplayComp(){
            if (isEmpty()){
                cout << "No hay elementos en el stack para mostrar" << endl;
                return;
            }
            Node *current;
            current = top;
            string main_text = "";
            string comp_text = "";
            string separator = "";

            while (current != NULL){
                main_text = current->get_nucleotide().get_name() + main_text;
                comp_text = current->get_nucleotide().get_comp() + comp_text;
                separator += "|";
                current = current->get_next();
            }

            cout << "\nDNA\n" << endl;
            cout << main_text << "\n" << separator << "\n" << comp_text << "\n" << endl;
        }
};
