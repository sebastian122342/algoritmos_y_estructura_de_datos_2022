// Integrantes: Sebastian Bustamante, Magdalena Lolas y Diego Nuñez
#include "Stack.h"
#include <iostream>
#include <cmath>

using namespace std;

// Esta funcion toma 3 nodos (2 numeros y una operacion),
// los opera y devuelve 1 nodo con el resultado
Node *merge (Node *num1, Node *num2, Node *op){
    // Se consideran floats en caso de que hayan divisiones, para que el resultado se exacto.
    float n1 = stof(num1->get_token().getValue());
    float n2 = stof(num2->get_token().getValue());
    float res;

    if (op->get_token().getValue() == "+") {
       res = n2 + n1;
   } else if (op->get_token().getValue() == "-") {
       res = n2 - n1;
   } else if (op->get_token().getValue() == "*"){
       res = n2 * n1;
   } else if (op->get_token().getValue() == "/"){
       res = n2 / n1;
   }else if (op->get_token().getValue() == "^"){
       res = pow(n2, n1);
   }

    // Una vez usados los 3 nodos, se libera su espacio en memoria, ya que ya cumplieron su funcion
    delete num1;
    delete num2;
    delete op;

    // Se crea el nodo con el resultado y se retorna
    Node *n = new Node(Token(to_string(res)));
    return n;
}

/*
   Esta es funcion recursiva que calcula una expresion matematica. Para esto recive la
   expresion con sus componentes (numeros y operaciones) en un stack previamente invertido.
   La funcion luego distribuye los contenidos de este stack en 2 nuevos stacks:
   uno para los numeros y otro para los operadores, como se indica en el algoritmo
   de la guia. Cuando encuentra un parentesis, esta funcion almacena sus contenidos
   en un nuevo stack auxiliar, y cuando el parentesis termina, la funcion se llama a si
   misma (recursion) pensando en el contenido del parentesis como si fuese una expresion
   distinta por separado. La funcion en este caso calcularia el resultado, y si hubiesen mas
   parentesis, los resolveria de forma recursiva hasta obtener un resultado.
   Una vez la recursion finaliza, el resultado del parentesis se añade como nodo al stack de
   numeros y se continua con el calculo. Cuando toda la expresion es calculada, se devuelve su valor.
*/
Node *calculate(Stack reversed_exp) {
    // Se crean los 3 stacks. Los 2 primeros almacenan los numeros y operadores respectivamente.
    // el Stack aux es una pila auxiliar que se utiliza en caso de llamar una recursion.
    Stack *stack_nums = new Stack();
    Stack *stack_ops = new Stack();
    Stack *aux = new Stack();

    // current representara el componente actual de la expresion que se esta evaluando.
    Node *current;

    // parenthesis representa la cantidad de parentesis
    // que han sido abiertos y que deben ser cerrados
    int parenthesis = 0;

    // tam representa el total de componentes de la expresion a calcular
    int tam;
    tam = reversed_exp.count();

    // Este ciclo distribuye los componentes de la expresion, para asi calcularla.
    for (int x = 0; x < tam; x++) {
        current = reversed_exp.pop();

        // Si se encuentra un abre parentesis, lo suma a la cantidad de parentesis por cerrar
        if (current->get_token().getValue() == "(") {
            parenthesis++;

            // Si ya hay un parentesis abierto, añade el nuevo "(" a
            // la pila aux para que sea considerado en la recursion
            if (parenthesis > 1)
                aux->push(current);

            continue;
        }

        // Esto solo se ejecuta si no hay parentesis abiertos
        if (parenthesis == 0) {
            // Si el componente que se esta validando es un numero,
            // se añade al stack de numeros (una prioridad de 0 indica un numero)
            if (current->get_token().getPriority() == 0) {
                stack_nums->push(current);

            // Si el componente es un operador se añade al stack de operadores
            } else {
                // Si no hay operadores en el stack, se añade directamente,
                // ya que no hay que comparar prioridades de operadores
                if (stack_ops->isEmpty()) {
                    stack_ops->push(current);

                // En caso de que hayan operadores en la pila, se comparan las prioridades del que
                // esta en el top de la pila con el que se desea añadir, y en caso de tener que
                // realizar un calculo, este se realiza y luego se añade el operador.
                } else if (stack_ops->get_top()->get_token().getPriority() >= current->get_token().getPriority()) {
                    // Se reduce lo que ya existe para luego agregar el operador nuevo.
                    while (stack_nums->count() > 1){
                        Node* num1 = stack_nums->pop();
                        Node* num2 = stack_nums->pop();
                        Node* op = stack_ops->pop();
                        Node *n;
                        n = merge(num1, num2, op);
                        stack_nums->push(n);
                    }
                }
                // Finalmente se añade el nuevo operador
                stack_ops->push(current);
            }

        // Esto se ejecuta cuando hay al menos 1 parentesis abierto sin cerrar
        } else {
            // Si encuentra un cierra parentesis, disminuye en 1 la cantidad que falta por cerrar
            if (current->get_token().getValue() == ")") {
                parenthesis--;
                // Si al cerrar el parentesis, no quedan mas por cerrar, se calcula lo que hay dentro
                if (parenthesis == 0) {
                    aux->reverse();
                    Node *n;

                    // La funcion se llama a si misma
                    n = calculate(*aux);

                    // El resultado del parentesis se añade como nodo a la pila de numeros.
                    stack_nums->push(n);

                    // Una vez terminada la recursion, la pila auxiliar se limpia,
                    // quedando asi disponible para posibles recusrsiones futuras
                    aux->clean();
                    continue;
                }
            }

            // Si hay un parentesis abierto, el componente actual se añade a la pila auxiliar.
            aux->push(current);
        }
    }

    // Si quedaron operaciones por resolver al finalizar el reparto del stack, se calculan aca
    while (stack_nums->count() > 1) {
        Node* num1 = stack_nums->pop();
        Node* num2 = stack_nums->pop();
        Node* op = stack_ops->pop();
        Node *n;
        n = merge(num1, num2, op);
        stack_nums->push(n);
    }
    return stack_nums->pop();
}


Stack string_to_stack(string input) {
    Stack exp;
    string text = "";

    // Este ciclo convierte la expresion ingresada de un string a un stack
    // con cada uno de los componentes, analizando caracter por caracter.
    for (int x = 0; x < input.length(); x++) {
        // Si el caracter es un digito lo añade al texto y sigue buscando
        // mas digitos hasta que encuentra algo que no sea un numero.
        if (isdigit(input[x])) {
            text += input[x];
        } else {
            // Cuando encuentra algo distinto a un digito, añade
            // un nodo de numero si es que hay texto guardado
            if (text != "") {
                Node *num = new Node(Token(text));
                exp.push(num);
            }

            // Luego añade el componente no numerico
            string s;
            s += input[x];
            Node *op = new Node(Token(s));

            exp.push(op);
            text = "";
        }
    }

    // Al finalizar el analisis del input, puede que queden numeros sin alñadir. Eso se resuelve aqui.
    if (text != "") {
        Node *num = new Node(Token(text));
        exp.push(num);
    }
    return exp;
}


int main() {
    Stack expression;
    string input;

    cout << "Ingrese la expresion algebraica a calcular: " << endl;
    cin >> input;

    expression = string_to_stack(input);

    // A la hora de pasar la expresion de string a stack, la misma se invierte
    // en el proceso, por lo tanto es necesario invertirla nuevamente
    expression.reverse();
    expression.DisplayStack();

    Node *result;

    // Se calcula la expresion
    result = calculate(expression);

    cout << "\nEl resultado de " + input + " es: " << result->get_token().getValue() << endl;

    return 0;
}
