/* Binary tree node */

#include "student.h"

class BinaryNode{
	public:
		Student student;
		BinaryNode *left;
		BinaryNode *right;

		BinaryNode(Student st): student(st), left(NULL), right(NULL){};
};
