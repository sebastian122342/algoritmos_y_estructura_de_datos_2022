/* Main cosa arboles binarios */

#include <iostream>
#include "binarytree.h"

using namespace std;

int main(){

    BinaryTree tree;

    // hacer un arbol x y rellenar
    Student s1 = Student(5.7, "hola1");
    Student s2 = Student(5.6, "hola2");
    Student s3 = Student(7, "hola3");
    Student s4 = Student(3.5, "hola4");
    Student s5 = Student(6.6, "hola5");
    Student s6 = Student(1.2, "hola6");
    Student s7 = Student(6.7, "hola7");
    Student s8 = Student(5.8, "hola8");
    Student s9 = Student(1.2, "hola9");

    tree.add(s1);
    tree.add(s2);
    tree.add(s3);
    tree.add(s4);
    tree.add(s5);
    tree.add(s6);
    tree.add(s7);
    tree.add(s8);
    tree.add(s9);

    cout << "\npostorder" << endl;
    tree.postorder(tree.root);
    cout << "\ninorder" << endl;
    tree.inorder(tree.root);
    cout << "\npreorder" << endl;
    tree.preorder(tree.root);
    cout << endl;

    //parece que asi esta bien

    return 0;
}
