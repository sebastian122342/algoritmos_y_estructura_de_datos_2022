/* BinaryTree */

#include "node.h"
using std::cout;

class BinaryTree{

    BinaryNode *insert(BinaryNode *new_root, Student student){
        if (new_root == NULL){
            return new BinaryNode(student);
        }else{
            if (student.get_average() >= new_root->student.get_average())
                new_root->left = insert(new_root->left, student);
            else
                new_root->right = insert(new_root->right, student);
        }
        return new_root;
    }

	public:

        BinaryNode *root;

		BinaryTree(): root(NULL) {};

        void add(Student student){
            root = insert(root, student);
        }

        void preorder(BinaryNode* ptr){
            if (ptr != NULL){
                cout<<" "<<ptr->student.get_average();
                preorder(ptr->left);
                preorder(ptr->right);
            }
        }

        void inorder(BinaryNode* ptr){
            if (ptr!=NULL){
                inorder(ptr->left);
                cout<<" "<<ptr->student.get_average();
                inorder(ptr->right);
            }
        }

        void postorder(BinaryNode* ptr){
            if (ptr != NULL){
                postorder(ptr->left);
                postorder(ptr->right);
                cout<<" "<<ptr->student.get_average();
            }
        }
};
