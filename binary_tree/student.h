/* Student */

#include <string>
using std::string;

class Student{
    float average;
	string tuition;

	public:
        Student(float avg, string tuit): average(avg), tuition(tuit){};

        void set_average(float average){
            this->average = average;
        }
        void set_tuition(string tuition){
            this->tuition = tuition;
        }

        float get_average(){
            return average;
        }
        string get_tuition(){
            return tuition;
        }
};
