#include<iostream>
#include<string.h>

using namespace std;

class Estudiante{
    private:
        char nombre[50];
        int edad;

    public:
        Estudiante(char *nombre, int edad){
            strcpy(this->nombre, nombre);
            this->edad = edad;
        }

        char *getNombre(){
            return nombre;
        }

        void setNombre(char *nombre){
            strcpy(this->nombre, nombre);
        }

        ///Lo mismo para la edad...

};

int main(){
    Estudiante p("Thomas Aranguiz", 19);

    cout << "Hola " << p.getNombre() << endl;

}
