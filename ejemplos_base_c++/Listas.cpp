#include<iostream>
#include<string>

using namespace std;

class Estudiante{
    private:
        string nombre;
        int edad;

    public:
        Estudiante(string nombre, int edad){
            this->nombre = nombre;
            this->edad = edad;
        }

        Estudiante(){
            this->nombre = "";
            this->edad = 0;
        }

        string getNombre(){
            return nombre;
        }

        void setNombre(string nombre){
            this->nombre = nombre;
        }

        int getEdad(){
            return edad;
        }

        void setEdad(int edad){
            this->edad = edad;
        }
};


//Por comodidad, es mejor definir la clase "Nodo" para luego definir la lista
//(cada nodo esta formado por los datos y por su "apuntador" al siguiente)
class NodoEstudiante{
    private:
        Estudiante valor;
        NodoEstudiante *vecino;

    public:
        NodoEstudiante(){
            this->vecino = NULL;
        }

        NodoEstudiante(Estudiante nuevo){
            this->valor.setNombre(nuevo.getNombre());
            this->valor.setEdad(nuevo.getEdad());

            this->vecino = NULL;
        }

        void setVecino(NodoEstudiante *vec){
            this->vecino = vec;
        }

        NodoEstudiante *getVecino(){
            return(this->vecino);
        }

        Estudiante *getEstudiante(){
            return(&this->valor);
        }

};

class ListaEstudiantes{
    private:
        NodoEstudiante *primero, *ultimo;

    public:
        ListaEstudiantes(){
            primero = ultimo = NULL;
        }

        void insertarNodo(NodoEstudiante *nuevo){
            if (ultimo == NULL){
                primero = ultimo = nuevo;
            }else{
                ultimo->setVecino(nuevo);
                ultimo = nuevo;
            }
        }

        void mostrarLista(){
            NodoEstudiante *recorrer;

            recorrer = primero;

            while(recorrer!=NULL){
                Estudiante *estudiante;
                estudiante = recorrer->getEstudiante();

                cout <<"Estudiante" << endl;
                cout <<"-- Nombre: " << estudiante->getNombre()  << endl;
                cout <<"-- Edad: " << estudiante->getEdad() << endl;

                recorrer = recorrer->getVecino();
           }
        }

        bool buscarEstudiante(string nombre){
            NodoEstudiante *recorrer;
            recorrer = primero;

            while(recorrer!=NULL){
                Estudiante *estudiante;
                estudiante = recorrer->getEstudiante();

                if (estudiante->getNombre() == nombre){
                    return true;
                }else{
                    return false;
                }
            }
        }

        void insertarPrimero(NodoEstudiante *nuevo){
            nuevo->setVecino(primero);
            primero = nuevo;
        }

        void quitarUltimo(){
            if (ultimo != NULL){
                NodoEstudiante *recorrer;
                recorrer = primero;

                while(recorrer!=NULL){

                    if (recorrer->getVecino()->getEstudiante() == ultimo->getEstudiante()){
                        ultimo = recorrer;
                        ultimo->setVecino(NULL);
                        break;
                    }
                }
            }else{
                cout << "No se puede" << endl;
            }
        }

        void quitarPrimero(){
            if (primero != NULL){
                NodoEstudiante *segundo;
                segundo = primero->getVecino();
                primero = segundo;
            }else{
                cout << "no se puede" << endl;
            }
        }
};

int main(){
    ListaEstudiantes lista;
    Estudiante p("Estudiante 1", 20);
    NodoEstudiante *nuevo = new NodoEstudiante(p);
    //lista.insertarNodo(nuevo);

    Estudiante q("Estudiante 2", 30);
    NodoEstudiante *nuevo2= new NodoEstudiante(q);
    //lista.insertarNodo(nuevo2);

    lista.mostrarLista();

    Estudiante r("Estudiante 3", 45);
    NodoEstudiante *nuevo3 = new NodoEstudiante(r);

    //lista.quitarUltimo();
    //lista.quitarPrimero();
    //bool existe = lista.buscarEstudiante("Estudiante 1");
    //lista.insertarPrimero(nuevo3);

    lista.mostrarLista();

    return 0;
}
