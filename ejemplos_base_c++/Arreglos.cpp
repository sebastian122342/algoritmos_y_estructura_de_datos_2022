#include<iostream>
#include<string.h>
#define N 10

using namespace std;

class Estudiante{
    private: 
        char nombre[50];
        int edad;

    public:
        Estudiante(char *nombre, int edad){
            strcpy(this->nombre, nombre);
            this->edad = edad;
        }

       Estudiante(){
            strcpy(this->nombre, "");
            this->edad = 0;
       }

        char *getNombre(){
            return nombre;
        }

        void setNombre(char *nombre){
            strcpy(this->nombre, nombre);
        }   

        int getEdad(){
            return edad;
        }

        void setEdad(int edad){
            this->edad = edad;
        }
};

int main(){
    //Para definir arreglo, es necesario reservar espacio para el total de elementos
    //(cada elemento tiene un tamaño que se calcula con la función sizeof)
    Estudiante *arreglo = (Estudiante *)malloc(sizeof(Estudiante) * N);
    // sizeof(Estudiante) * N) --> Determina el total de espacio que requiere los N estudiantes
    //malloc(sizeof(Estudiante) * N) --> Reserva el espacio para los N estudiantes y devuelve 
            //la dirección donde se inicia dicha reserva
    //(Estudiante *)malloc(sizeof(Estudiante) * N) --> hace "casting" para indicar que la dirección 
    //es para almacenar un estudiante
    
    arreglo[0] = Estudiante("Josefa ..", 19);
    arreglo[1] = Estudiante("Claudio La Rosa", 20);

    cout << "1er Estudiante es " << arreglo[0].getNombre() << endl;

     return 0;
}