/*
colas y pilas

Hacer pila de estudiantes con push y pop

*/

#include <iostream>
#include <string>
#include <cstdlib>
#include <time.h>

using std::cout;
using std::endl;
using std::string;
using std::rand;


class Student{
    private:
        string name;
        int age;

    public:
        Student(string name, int age){
            this->name = name;
            this->age = age;
        }

        Student(){
            this->name = "";
            this->age = 0;
        }

        string get_name(){
            return name;
        }

        void set_name(string name){
            this->name = name;
        }

        int get_age(){
            return age;
        }

        void set_age(int age){
            this->age = age;
        }
};


class Node{
    Student student;
    Node *next;

    public:
        Node(){
            next = NULL;
        }
        Node(Student student){
            this->student = student;
            this->next = NULL;
        }
        Node *get_next(){
            return next;
        }
        void set_next(Node *next){
            this->next = next;
        }
        void set_next(){
            next = NULL;
        }
        Student get_student(){
            return student;
        }
        void set_student(Student student){
            this->student = student;
        }
};


class Stack{

    Node* first;

    public:
        Stack(){
            first = NULL;
        }

        void pop(){
            if (first == NULL)
                return;
            Node * aux;
            aux = first;
            first = first->get_next();
            delete aux;
        }

        Student top(){
            return first->get_student();
        }

        void push(Node *pushed){
            pushed->set_next(first);
            first = pushed;
        }

        bool is_empty(){
            if (first == NULL)
                return true;
            return false;
        }

        void show(){
            int i = 1;
            Node *current;
            current = first;
            while (current != NULL){
                cout << i << ".-" << current->get_student().get_name() << endl;
                current = current->get_next();
                i++;
            }
        }
};


Node *create_student(){
    string names[] = {"Pedro", "Juan", "Jose", "Martin", "Carla", "Maria", "Flora"};
    string name = names[rand()%7];
    int age = rand() % 100 + 1;
    return new Node(Student(name, age));
}


int main(){

    srand(time(NULL));

    Stack stack;
    for (int i=0; i<10; i++)
        stack.push(create_student());
    stack.show();

    cout << "------------------------------------" << endl;

    for (int i=0; i<9; i++)
        stack.pop();
    //stack.pop();
    stack.show();
    return 0;
}
