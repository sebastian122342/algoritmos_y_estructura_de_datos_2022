/*
colas y pilas

Hacer pila de estudiantes con push y pop

*/

#include <iostream>
#include <string>
#include <cstdlib>
#include <time.h>

using std::cout;
using std::endl;
using std::string;
using std::rand;


class Student{
    private:
        string name;
        int age;

    public:
        Student(string name, int age){
            this->name = name;
            this->age = age;
        }

        Student(){
            this->name = "";
            this->age = 0;
        }

        string get_name(){
            return name;
        }

        void set_name(string name){
            this->name = name;
        }

        int get_age(){
            return age;
        }

        void set_age(int age){
            this->age = age;
        }
};


class Node{
    Student student;
    Node *next;

    public:
        Node(){
            next = NULL;
        }
        Node(Student student){
            this->student = student;
            this->next = NULL;
        }
        Node *get_next(){
            return next;
        }
        void set_next(Node *next){
            this->next = next;
        }
        void set_next(){
            next = NULL;
        }
        Student get_student(){
            return student;
        }
        void set_student(Student student){
            this->student = student;
        }
};

class Queue{

    Node *first, *last;

    public:
        Queue(){
            first = last = NULL;
        }

        void push(){
            cout << "hola" << endl;
        }

        void pop(){
            if (first == NULL)
                return;
            Node * aux;
            aux = first;
            first = first->get_next();
            delete aux;
        }
}

int main(){

    return 0;
}
