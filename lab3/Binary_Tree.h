/* Clase arbol binario */
#include "Tree_Node.h"

using namespace std;

class BinaryTree {
    private:
        TreeNode *root;

        TreeNode *insert(TreeNode *root, Student s) {
            if (root == NULL) {
                return new TreeNode(s);

            } else {
                if (s.get_average() > root->student.get_average()) {
                    root->left = insert(root->left, s);
                } else {
                    root->right = insert(root->right, s);
                }
            }
            return root;
        }

        void rec_preorder(TreeNode *node) {
            if (node == NULL)
                return;
            cout << node->student.get_average() << " - ";
            rec_preorder(node->left);
            rec_preorder(node->right);
        }

        void rec_inorder(TreeNode *node) {
            if (node == NULL)
                return;

            rec_preorder(node->left);
            cout << node->student.get_average() << " - ";
            rec_preorder(node->right);
        }

        void rec_postorder(TreeNode *node) {
            if (node == NULL)
                return;

            rec_postorder(node->left);
            rec_postorder(node->right);
            cout << node->student.get_average() << " - ";
        }

    public:

        BinaryTree() : root(NULL) {}

        void add(Student s) {
            root = insert(root, s);
        }

        void preorder() {
            cout << "\npre order: " << endl;
            rec_preorder(root);
        }

        void inorder() {
            cout << "\nin order: " << endl;
            rec_inorder(root);
        }

        void postorder() {
            cout << "\npost order: " << endl;
            rec_postorder(root);
        }
};
