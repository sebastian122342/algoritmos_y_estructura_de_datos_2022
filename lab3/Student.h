/* Clase estudiante con matricula y promedio */
#include <iostream>

using namespace std;

class Student{
	private:
		float average;
		string tag;

	public:
		Student(float x, string m) : average(x), tag(m) {}

		float get_average(){
			return average;
		}

        string get_tag(){
            return tag;
        }

};
