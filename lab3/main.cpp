// Integrantes: Sebastian Bustamante, Magdalena Lolas y Diego Nuñez


#include "Binary_Tree.h"
#include "Menu.h"

using namespace std;

// Plantilla de funcion para recibir un dato correctamente
template <typename T>
T ask_option(T data){
    while(!(cin >> data)){
        cout << "Ingresar correctamente: ";
        cin.clear();
        cin.ignore(100, '\n');
    }
    return data;
}

int main() {

    // Creacion del arbol binario y menu con sus opciones
    BinaryTree tree;
    string options[] = {"Ingresar nuevo registro", "Mostrar in-order",
                        "Mostrar pre-order", "Mostrar post-order", "Salir"};
    Menu menu(options);

    // Se crean 4 estudiantes de base
    Student e1(4.7, "1");
    Student e2(3.0, "2");
    Student e3(2.7, "3");
    Student e4(6.1, "4");

    // Se inicializa el arbol binario con los datos de estos 4 estudiantes
    tree.add(e1);
    tree.add(e2);
    tree.add(e3);
    tree.add(e4);


    while (true){
        cout << "\n" << endl;
        menu.show_options();
        int option = menu.ask_option();
        switch (option) {
            case 1: // agrega un nuevo estudiante
                {
                    string tuition;
                    float average;
                    cout << "Ingresar matricula: ";
                    tuition = ask_option<string>(tuition);
                    cout << "Ingresar promedio: ";
                    average = ask_option<float>(average);
                    Student x(average, tuition);
                    tree.add(x);
                }
                break;
            case 2: // muestra in order
                tree.inorder();
                break;
            case 3: // muestra pre order
                tree.preorder();
                break;
            case 4: // muestra post order
                tree.postorder();
                break;
            case 5: // termina el programa
                return 0;
            default:
                continue;
        }
    }

    cout << "" << endl;

    return 0;
}
