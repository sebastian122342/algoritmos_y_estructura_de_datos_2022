/* Clase menu, muestra opciones y pide decision */

#include <iostream>

using namespace std;

class Menu{
	private:
		int total_opt = 5;
		string options[5];

	public:
		Menu(string opts[]){
			for (int i=0; i < total_opt; i++)
				options[i] = opts[i];
		}

		void show_options(){
			for (int i=0; i < total_opt; i++)
				cout << "<" << i+1 << "> " << options[i] << endl;
		}

		int ask_option(){
			int option;
			cout << "Escoja una opcion: ";
			while(!(cin >> option)){
				cout << "Debe ser un numero: ";
				cin.clear();
				cin.ignore(100, '\n');
			}
			return option;
		}
};
