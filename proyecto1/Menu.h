/* Clase menu, muestra opciones y pide decision */

#include <iostream>

using namespace std;

class Menu{
	private:
		int total_opc;
		string *opciones;

	public:
		Menu(string opcs[], int total){
            total_opc = total;
			opciones = new string[total_opc];
            for (int i=0; i < total_opc; i++)
                opciones[i] = opcs[i];
        }

        ~Menu(){
            delete [] opciones;
        }

		void mostrar_opciones(){
			for (int i=0; i < total_opc; i++)
				cout << "<" << i+1 << "> " << opciones[i] << endl;
		}

		int pedir_opcion(){
			int opcion;
			cout << "Escoja una opcion: ";
			while(!(cin >> opcion)){
				cout << "Debe ser un numero valido: ";
				cin.clear();
				cin.ignore(100, '\n');
			}
			return opcion;
		}
};
