// Clase GrafoTipo con constantes a utilizar

#include <string>

const int NULL_ARISTA = 0;
const int INF = 999999;

template <class T>
class GrafoTipo {
	public:
        GrafoTipo(int);
        ~GrafoTipo();
        void AgregaVertice(T);
        void AgregaArista(T, T, float);
        float CostoEs(T, T);
        int IndiceEs(T*, T);
        bool EstaVacio();
        bool EstaLleno();
        int TamanioGrafo();
        void MostrarSolucion();
        int DistMinima();
        void dijkstra(T);
        float redondear(float);

    private:
        int numVertices;
        int maxVertices;

        T* vertices;
        float **aristas;

        bool* marcas;
        float *dists;
};
