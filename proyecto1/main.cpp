// Proyecto 1
// Integrantes: Sebastian Bustamante, Magdalena Lolas y Diego Nuñez

#include <cstdlib>
#include "grafo.cpp"
#include "Menu.h"

int main(){

    // Se crea el grafo y se agregan los metabolitos
    int tam = 33;
    string metabolitos[tam] = {"Glucosa", "Glucosa-6-P", "Fructosa-6-P", //3
    "Gliceraldehido-3-P", "Dihidroxiacetona-Fosfato", "1,3-Difosfoglicerato", // 6
    "3-Fosfoglicerato", "2-Fosfoglicerato", "Fosfoenol Piruvato", // 9
    "Piruvato", "Lactato", "Oxaloacetato", "Fructosa-1,6-bifosfato", "Lactosa", // 14
    "Galactosa", "Glucosa-1-P", "Sacarosa", "Fructosa", "Fructosa-1-P", // 19
    "Gliceraldehido", "Lactato", "6-Fosfogluconato", "Ribulosa-5-P", "Ribosa-5-P", // 24
    "Acetil-CoA", "Oxaloacetato", "Citrato", "Isocitrato", "a-Cetoglutarato", // 29
    "Succinil-CoA", "Succinato", "Fumarato", "Malato" // 33
    };

    GrafoTipo<string> metabolismo(tam);
    for (int i=0; i < tam; i++)
        metabolismo.AgregaVertice(metabolitos[i]);

    // Glicolisis
    metabolismo.AgregaArista("Glucosa", "Glucosa-6-P", -4);
    metabolismo.AgregaArista("Glucosa-6-P", "Fructosa-6-P", 0.4);
    metabolismo.AgregaArista("Fructosa-6-P", "Fructosa-1,6-bifosfato", -3.3);
    metabolismo.AgregaArista("Fructosa-1,6-bifosfato", "Gliceraldehido-3-P", 5.7);
    metabolismo.AgregaArista("Fructosa-1,6-bifosfato", "Dihidroxiacetona-Fosfato", 5.7);
    metabolismo.AgregaArista("Dihidroxiacetona-Fosfato", "Gliceraldehido-3-P", 1.8);
    metabolismo.AgregaArista("Gliceraldehido-3-P", "1,3-Difosfoglicerato", 1.5);
    metabolismo.AgregaArista("1,3-Difosfoglicerato", "3-Fosfoglicerato", -4.5);
    metabolismo.AgregaArista("3-Fosfoglicerato", "2-Fosfoglicerato", 1.06);
    metabolismo.AgregaArista("2-Fosfoglicerato", "Fosfoenol Piruvato", 0.4);
    metabolismo.AgregaArista("Fosfoenol Piruvato", "Piruvato", -7.5);
    metabolismo.AgregaArista("Piruvato", "Acetil-CoA", -5.3);

    // Gluconeogenesis
    metabolismo.AgregaArista("Fructosa-6-P", "Glucosa-6-P", -0.4);
    metabolismo.AgregaArista("Gliceraldehido-3-P", "Fructosa-1,6-bifosfato", -5.7);
    metabolismo.AgregaArista("Gliceraldehido-3-P", "Dihidroxiacetona-Fosfato", -1.8);
    metabolismo.AgregaArista("1,3-Difosfoglicerato", "Gliceraldehido-3-P", -1.5);
    metabolismo.AgregaArista("3-Fosfoglicerato", "1,3-Difosfoglicerato", 4.5);
    metabolismo.AgregaArista("2-Fosfoglicerato", "3-Fosfoglicerato", -1.06);
    metabolismo.AgregaArista("Fosfoenol Piruvato", "2-Fosfoglicerato", -0.4);
    metabolismo.AgregaArista("Piruvato", "Oxaloacetato", -0.5);
    metabolismo.AgregaArista("Oxaloacetato", "Fosfoenol piruvato", 7);
    metabolismo.AgregaArista("Fructosa-1,6-bifosfato", "Fructosa-6-P", 3.3);
    metabolismo.AgregaArista("Glucosa-6-P", "Glucosa", 4);

    // Azucares varios
    metabolismo.AgregaArista("Lactosa", "Glucosa", 6.2);
    metabolismo.AgregaArista("Lactosa", "Galactosa", 6.2);
    metabolismo.AgregaArista("Galactosa", "Glucosa-1-P", -3.1);
    metabolismo.AgregaArista("Glucosa-1-P", "Glucosa-6-P", 0.8);
    metabolismo.AgregaArista("Sacarosa", "Glucosa", 5.3);
    metabolismo.AgregaArista("Sacarosa", "Fructosa", 5.3);
    metabolismo.AgregaArista("Fructosa", "Fructosa-6-P", -4.7);
    metabolismo.AgregaArista("Fructosa", "Fructosa-1-P", -4.5);
    metabolismo.AgregaArista("Fructosa-1-P", "Gliceraldehido", 2.4);
    metabolismo.AgregaArista("Fructosa-1-P", "Dihidroxiacetona-Fosfato", 2.7);
    metabolismo.AgregaArista("Gliceraldehido", "Gliceraldehido-3-P", -5.1);

    // Fermentacion
    metabolismo.AgregaArista("Piruvato", "Lactato", -6);

    // Via de las pentosas Fosfato
    metabolismo.AgregaArista("Glucosa-6-P", "6-Fosfogluconato", -4.4);
    metabolismo.AgregaArista("6-Fosfogluconato", "Ribulosa-5-P", -2.6);
    metabolismo.AgregaArista("Ribulosa-5-P", "Glucosa-6-P", 5.2);
    metabolismo.AgregaArista("Ribulosa-5-P", "Ribosa-5-P", -1.9);

    // Ciclo de krebs
    metabolismo.AgregaArista("Acetil-CoA", "Citrato", -2.1);
    metabolismo.AgregaArista("Oxaloacetato", "Citrato", -1.7);
    metabolismo.AgregaArista("Citrato", "Isocitrato", -0.85);
    metabolismo.AgregaArista("Isocitrato", "a-Cetoglutarato", 0.6);
    metabolismo.AgregaArista("a-Cetoglutarato", "Succinil-CoA", 3.8);
    metabolismo.AgregaArista("Succinil-CoA", "Succinato", -4.1);
    metabolismo.AgregaArista("Succinato", "Fumarato", -5.3);
    metabolismo.AgregaArista("Fumarato", "Malato", -1.5);
    metabolismo.AgregaArista("Malato", "Oxaloacetato", 1.2);

    // Menu para decidir si ir por el camino mas corto entre dos puntos o salir
    string opcs1[] = {"Calcular camino mas corto", "Salir"};
    Menu menu1(opcs1, 2);
    while (true){
        cout << "\n" << endl;
        menu1.mostrar_opciones();
        int opc1 = menu1.pedir_opcion();
        switch (opc1) {
            case 1:
            {
                Menu menu2(metabolitos, tam);
                menu2.mostrar_opciones();
                int desde;
                do{
                    desde = menu2.pedir_opcion();
                }while (desde <= 0 || desde >= tam+1);

                string met_ini = metabolitos[desde-1];
                metabolismo.dijkstra(met_ini);
                break;
            }
            case 2:
                exit(0);
            default:
                continue;
        }
    }
    return 0;
}
