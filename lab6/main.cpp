// Laboratorio 6 - Magdelena Lolas - Diego Núñez - Sebastián Bustamante

#include "Menu.h"
#include <chrono>
using namespace std::chrono;

// Inicializa un arreglo con valores 0
void init_array(int array[], int size){
    for (int i=0; i < size; i++)
        array[i] = 0;
}

// Mostrar el arreglo
void show_array(int array[], int size){
    string text = "";
    for (int i=0; i < size; i++)
        text += " " + to_string(array[i]);
    cout << "Arreglo:" << text << endl;
}

// Intercambio del valor de dos variables
void swap(int* x, int* y){
    int temp;
    temp = *x;
    *x = *y;
    *y = temp;
}

// Ordenamiento burbuja
void bubble_sort(int array[], int size){
    for (int i=0; i < size-1; i++){
        for (int j=0; j < size-i-1; j++){
            if (array[j] > array[j+1]){
                swap(&array[j], &array[j+1]);
            }
        }
    }
}

// Se vuelve al estado original, desordenado
void disarray(int array[], int copy[], int size){
    for (int i=0; i < size; i++)
        array[i] = copy[i];
}

// Se mezclan dos arregos en uno solo
void merge(int array[], int const left, int const center, int const right){
    // Tamaños de los arreglos izquierdo y derecho
    auto const size_larray = center - left + 1;
    auto const size_rarray = right - center;

    // Se crean los arreglos izquierdo y derecho
    auto *left_array = new int[size_larray], *right_array = new int[size_rarray];

    // Se asignan valores a arreglos izquierdo y derecho
    for (auto i = 0; i < size_larray; i++)
        left_array[i] = array[left + i];
    for (auto j = 0; j < size_rarray; j++)
        right_array[j] = array[center + 1 + j];

    auto left_index = 0, right_index = 0;
    int merged_index = left;

    // Se mezclan los arreglos en uno solo, en orden. Se van aumentando los
    // valores de los indices segun se va avanzando.
    // Mientras no se salga de los bordes de los arreglos temporales
    while (left_index < size_larray && right_index < size_rarray){
        // Si el valor del arreglo izquierdo es menor que el del derecho,
        // agregar este valor al arreglo original.
        if (left_array[left_index] <= right_array[right_index]){
            array[merged_index] = left_array[left_index];
            left_index++;
        }
        // Si no, agregar el valor del arreglo derecho
        else{
            array[merged_index] = right_array[right_index];
            right_index++;
        }
        merged_index++;
    }

    // Se siguen agregando valores del arreglo izquierdo mientras aun hayan
    while (left_index < size_larray){
        array[merged_index] = left_array[left_index];
        left_index++;
        merged_index++;
    }

    // Se siguen agregando valores del arreglo derecho mientras aun hayan
    while (right_index < size_rarray){
        array[merged_index] = right_array[right_index];
        right_index++;
        merged_index++;
    }

    // Se eliminan los arreglos que se crearon de forma temporal
    delete [] left_array;
    delete [] right_array;
}

// Ordenamiento por mezclas
void merge_sort(int array[], int const start, int const end) {
    // Caso base: recursion, ya no se puede dividir mas
    if (start >= end)
        return;

    // Se divide el arreglo en dos, recursivamente
    auto center = start + (end - start) / 2;
    merge_sort(array, start, center);
    merge_sort(array, center + 1, end);
    merge(array, start, center, end);
}

int main(){

    int size;
    cout << "Ingrese el tamaño del arreglo: ";
    cin >> size;
    int array[size];
    int copy[size];
    init_array(array, size); // Se inicializa el arreglo
    // Ingresar elementos del arreglo, se guarda un arreglo copia
    cout << "Ingrese los elementos del arreglo" << endl;
    int num;
    for (int i=0; i<size; i++){
        cout << "Elemento " << i+1 << ": ";
        cin >> num;
        array[i] = num;
        copy[i] = num;
    }
    show_array(array, size);
    // Menu con las acciones requeridas
    string opciones[] = {"Mostrar arreglo original sin ordenar",
    "Ordenar y mostrar con bubble sort", "Ordenar y mostrar con merge sort",
    "Tiempos de ejecucion de los algoritmos", "Salir"};
    Menu menu(opciones, 5);
    while (true){
        menu.show_options();
        int des = menu.ask_option();
        switch (des){
            case 1: // Mostrar sin ordear
                show_array(copy, size);
                break;
            case 2: // Bubble sort
                bubble_sort(array, size);
                show_array(array, size);
                disarray(array, copy, size);
                break;
            case 3: // Merge sort
                merge_sort(array, 0, size-1);
                show_array(array, size);
                disarray(array, copy, size);
                break;
            case 4: // Medicion de los tiempos
            {
                // Tiempo de bubble sort
                auto start = high_resolution_clock::now();
                bubble_sort(array, size);
                auto stop = high_resolution_clock::now();
                auto duration = duration_cast<microseconds>(stop - start);
                cout << "Duracion del algoritmo burbuja: " << duration.count() << " ms" << endl;

                disarray(array, copy, size);

                // Tiempor de merge sort
                auto start2 = high_resolution_clock::now();
                merge_sort(array, 0, size-1);
                auto stop2 = high_resolution_clock::now();
                auto duration2 = duration_cast<microseconds>(stop2 - start2);
                cout << "Duracion del algoritmo burbuja: " << duration2.count() << " ms" << endl;

                disarray(array, copy, size);
                break;
            }
            case 5: // Salir
                exit(0);
            default:
                continue;
        }
    }
    return 0;
}
