/* Clase menu, muestra opciones y pide decision */

#include <iostream>

using namespace std;

class Menu{
	private:
		int total_opt;
		string *options;

	public:
		Menu(string opts[], int total){
            total_opt = total;
			options = new string[total_opt];
            for (int i=0; i < total_opt; i++)
                options[i] = opts[i];
        }

        ~Menu(){
            delete [] options;
        }

        // Se muestras las opciones del menu
		void show_options(){
			for (int i=0; i < total_opt; i++)
				cout << "<" << i+1 << "> " << options[i] << endl;
		}

        // Se pide una opcion. Se valida.
		int ask_option(){
			int option;
			cout << "Escoja una opcion: ";
			while(!(cin >> option)){
				cout << "Debe ser un numero valido: ";
				cin.clear();
				cin.ignore(100, '\n');
			}
			return option;
		}
};
