// Ejemplo clase estudiante

#include<iostream>
#include<string.h>

using namespace std;

// clase hecha en la clase
class Estudiante{
    private:
        char nombre[50];
        int edad;

    public:
        // Equivalente el init, es la clase constructora
        Estudiante(char *nombre, int edad){
            strcpy(this->nombre, nombre);
            this -> edad = edad;
        }

        // getter de nombre y edad
        char *get_name(){
            return nombre;
        }
        int get_age(){
            return edad;
        }

        // Setter de nombre y edad
        void set_nombre(char *nombre){
            strcpy(this->nombre, nombre);
        }
        void set_edad(int *edad){
            this -> edad = *edad;
        }
};

// clase propia
class Student{
    private:
        int age;
        string name;

    public:
        // constructor
        Student(string name, int age){
            cout << "Iniciando clase" << endl;
            this->age = age;
            this->name = name;
        }

        int get_age(){
            return age;
        }
        string get_name(){
            return name;
        }
};

int main(){

    // prohibido usar printf
    Estudiante p("pepe", 14);
    cout << "hola " << p.get_name() << " tienes " << p.get_age() << " años " << endl;

    Student juan("Juan", 19);
    cout << "Mi nombre es " << juan.get_name() << " y tengo " << juan.get_age() << " años" << endl;


    return 0;
}
