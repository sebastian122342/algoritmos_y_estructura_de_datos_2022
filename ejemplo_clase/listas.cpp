// tema listas enlazadas

#include<iostream>
#include<string.h>

using namespace std;


class Estudiante{
    private:
        char nombre[50];
        int edad;

    public:
        // Equivalente el init, es la clase constructora
        Estudiante(char *nombre, int edad){
            strcpy(this->nombre, nombre);
            this -> edad = edad;
        }

        // getter de nombre y edad
        char *get_nombre(){
            return nombre;
        }
        int get_edad(){
            return edad;
        }

        // Setter de nombre y edad
        void set_nombre(char *nombre){
            strcpy(this->nombre, nombre);
        }
        void set_edad(int *edad){
            this -> edad = *edad;
        }
};

class ListaEstudiantes{
    private:
        Estudiante valor;
        ListaEstudiantes *sig;

    public:
        ListaEstudiantes(){
            valor = Estudiante();
            sig = NULL;
        }

        ListaEstudiantes(Estudiante nuevo){
            // eso no funciona porque es 1 a 1 cada atributo no se puede
            // asignar de golpe
            valor = nuevo;
            sig = NULL;
    }

    char *getNombreValor(){
        return valor
    }


};

int main(){

    ListaEstudiantes *primero, *ultimo;
    ListaEstudiantes *nuevo;

    Estudiante p;
    p.set_nombre("hola")

    nuevo = new ListaEstudiantes(p);
    primero = nuevo;
    ultimo = nuevo;

    cout << primero->getNombreValor() << endl;


    return 0;
}
