/*
Stack class
*/

#include <iostream>
#include <cstdlib>
#include <time.h>
#include "node.h"
#include "menu.h"

using std::rand;

/*

mirar al stack dado vuelta, el top es el primero
y solamente se van sacando elementos desde abajo

el1               el1
el2    -------->  el2    ahora el top es el3
el3     pop();    el3
el4

el1                   el1
el2    -------->      el2    ahora el top es el5
el3     push(el5);    el3
el4                   el4
                      el5

Para mostrar los elemementos del stack, se hace de la forma tradicional y
el se mostrara mas arriba mientras que los demas elemenos abajo, al reves
de como se mostro anteriormente.

el1                     el4
el2    -------->        el3    Se mostrara asi
el3     DisplayStack()  el2
el4                     el1


*/

class Stack{
    Node *top;
    public:

        Stack(){
            top = NULL;
        }

        Student get_top(){
            Student blank;
            if (isEmpty())
                return blank;
            return top->get_student();
        }

        Student pop(){
            Student blank;
            if (isEmpty())
                return blank;
            Node *aux = top;
            top = top->get_next();
            return aux->get_student();
        }

        void push(Node *new_top){
            new_top->set_next(top);
            top = new_top;
        }

        bool isEmpty(){
            if (top == NULL) return true;
            return false;
        }

        void DisplayStack(){
            // mostrar de la forma tradicional, no al reves como se interpreto
            if (isEmpty()){
                cout << "No hay elementos en el stack para mostrar" << endl;
                return;
            }
            Node *current;
            current = top;
            string stack_text = "";

            while (current != NULL){
                stack_text += "|\t"+ current->get_student().get_name() +"\t|"+ "\n";
                current = current->get_next();
            }

            cout << "\nStack" << endl;
            cout << "_________________" << endl;
            cout << stack_text;
            cout << "-----------------" << endl;
        }
};

Node *create_student(){
    string names[] = {"Pedro", "Juan", "Jose", "Martin", "Carla", "Maria", "Flora"};
    string name = names[rand()%7];
    int age = rand() % 100 + 1;
    return new Node(Student(name, age));
}

int main(){

    srand(time(NULL));
    int stack_size = 10;
    string options[] = {"Mostrar elemento superior", "Agregar", "Eliminar",
                     "Mostrar Stack", "Salir"};
    Stack stack;
    Menu menu(options);

    // inicializa el stack con 10 estudiantes
    for (int i=0; i < stack_size; i++)
        stack.push(create_student());

    while (true){
        menu.show_options();
        int option = menu.ask_option();

        switch (option){
            case 1:
            {
                Student student;
                student = stack.get_top();
                if (!student.is_blank())
                    cout << student.get_name() << endl;
                else
                    cout << "No hay elementos" << endl;
                break;
            }
            case 2:
                stack.push(create_student());
                break;
            case 3:
            {
                Student student;
                student = stack.pop();
                if (!student.is_blank())
                    cout << "Se elimino " << student.get_name() << endl;
                else
                    cout << "No quedan elementos para eliminar" << endl;
                break;
            }
            case 4:
                stack.DisplayStack();
                break;
            case 5:
                exit(0);
            default:
                continue;
        }
    }

    return 0;
}
