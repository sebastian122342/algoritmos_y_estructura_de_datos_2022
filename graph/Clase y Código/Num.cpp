#include "Num.h" 

Num::Num(): num(0){}

/*
Num::Num(): {
    num = 0;
} 
*/

Num::Num(int n): num(n) {} 

/*
Num::Num(int n): {
    nume = n;
} 
*/

int Num::getNum() { 
    return num; 
} 