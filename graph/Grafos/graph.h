const int NULL_EDGE = 0;
class GraphType {
  public:
    GraphType(int);
    ~GraphType();
    void AddVertex(int);
    void AddEdge(int, int, int);
    int WeightIs(int, int);
    int IndexIs(int*, int);
    bool IsEmpty();
    bool IsFull(); 
    int GraphSize();

 private:
    int numVertices;
    int maxVertices;

    int* vertices;
    int **edges;

    bool* marks;
}; 
