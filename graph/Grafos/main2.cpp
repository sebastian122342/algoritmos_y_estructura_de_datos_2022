#include <iostream>
#include "graph.h"

using namespace std;

int main() {
    GraphType x(10);

    cout << "Vació? " << x.IsEmpty() << " - " << x.GraphSize() << endl;


    x.AddVertex(10);
    x.AddVertex(20);
    cout << "\nVació? " << x.IsEmpty() << " - " << x.GraphSize() << endl;
    x.AddEdge(10, 20, 100);
    cout << "Peso: " << x.WeightIs(10, 20) << endl;
    x.AddVertex(30);
    cout << "\nVació? " << x.IsEmpty() << " - " << x.GraphSize() << endl;
    x.AddEdge(10, 30, 200);
    cout << "Peso: " << x.WeightIs(10, 30) << endl;

}
